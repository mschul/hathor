# Hathor #

a simple software synthesizer using JUCE Library and some C++11.

* This is a juce Audio plugin, jucer file is included
* Some paths to SDKs (VST...) need to be adjusted.

 This is work in progress, the UI is virtually non existing. However, current features include:

* Quasi Bandlimited Oscillators
* LFOs
* Filters (currently only a linear LP/HP Filter)
* Envelope Generator
* Delay
