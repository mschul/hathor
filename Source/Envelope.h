#include "../JuceLibraryCode/JuceHeader.h"

#include "SynthComponent.h"

#ifndef ENVELOPE_H_INCLUDED
#define ENVELOPE_H_INCLUDED

namespace hathor
{
    class Envelope : public SynthComponent
    {
    public:
        typedef std::shared_ptr<Envelope> Ptr;
        enum Mode { ANALOG, DIGITAL };
        enum State { OFF, ATTACK, DECAY, SUSTAIN, RELEASE };

        Envelope() {}

        void init( int sampleRate, SynthComponent::Ptr mode,
                   SynthComponent::Ptr attackTime, SynthComponent::Ptr decayTime,
                   SynthComponent::Ptr sustainLevel, SynthComponent::Ptr releaseTime );

        void start();
        void stop();
        void turnOff();

        double getAttackTime() const { return params[attackTimeIdx]->getSignal().left; }
        double getDecayTime() const { return params[decayTimeIdx]->getSignal().left; }
        double getSustainLevel() const { return params[sustainLevelIdx]->getSignal().left; }
        double getReleaseTime() const { return params[releaseTimeIdx]->getSignal().left; }

        void onModeChange( Signal v );
        void onAttackTimeChange( Signal v );
        void onDecayTimeChange( Signal v );
        void onReleaseTimeChange( Signal v );

        State getState() const { return state; }
        void setState( State state ) { this->state = state; }

    protected:
        virtual signal renderInternal(Signal input) override;

        int modeIdx;

        int attackTimeIdx;
        double attackTimeScalar, attackTCO, attackCoeff, attackOffset;

        int decayTimeIdx;
        double decayTimeScalar, decayTCO, decayCoeff, decayOffset;

        int sustainLevelIdx;

        int releaseTimeIdx;
        double releaseTCO, releaseCoeff, releaseOffset;

        bool resetToZero = false;
        double envelope = 0.0;
        State state = OFF;
    };
}

#endif
