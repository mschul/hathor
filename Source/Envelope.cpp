#include "Envelope.h"
using namespace hathor;

void Envelope::init( int sampleRate, SynthComponent::Ptr mode,
                     SynthComponent::Ptr attackTime, SynthComponent::Ptr decayTime,
                     SynthComponent::Ptr sustainLevel, SynthComponent::Ptr releaseTime )
{
    SynthComponent::init( sampleRate );

    attackTimeScalar = 1.0;
    decayTimeScalar = 1.0;

    {
        // Mode
        param* p = new param( mode );
        p->setOnChangeCallback( static_cast<param::OnChangeCallbackPtr>( &Envelope::onModeChange ) );
        modeIdx = params.size();
        params.push_back( std::unique_ptr<param>( p ) );
    }
    {
        // AttackTime
        param* p = new param( attackTime );
        p->setOnChangeCallback( static_cast<param::OnChangeCallbackPtr>( &Envelope::onAttackTimeChange ) );
        attackTimeIdx = params.size();
        params.push_back( std::unique_ptr<param>( p ) );
    }
    {
        // DecayTime
        param* p = new param( decayTime );
        p->setOnChangeCallback( static_cast<param::OnChangeCallbackPtr>( &Envelope::onDecayTimeChange ) );
        decayTimeIdx = params.size();
        params.push_back( std::unique_ptr<param>( p ) );
    }
    {
        // SustainLevel
        param* p = new param( sustainLevel );
        sustainLevelIdx = params.size();
        params.push_back( std::unique_ptr<param>( p ) );
    }
    {
        // ReleaseTime
        param* p = new param( releaseTime );
        releaseTimeIdx = params.size();
        params.push_back( std::unique_ptr<param>( p ) );
    }
}


void Envelope::onModeChange( Signal v )
{
    // --- analog - use e^-5x
    if ( v.left == ANALOG )
    {
        // these are the e^-5x over/undershoot values
        // which will generate atching e^-5x curves
        attackTCO = exp( -0.5 ); // fast attack
        //	m_dAttackTCO = exp(-5.0);
        decayTCO = exp( -5.0 );
        releaseTCO = decayTCO;
    }
    else
    {
        // digital is linear-in-dB so use
        // the -96dB noise floor for over/undershoot
        attackTCO = pow( 10.0, -96.0 / 20.0 );
        decayTCO = attackTCO;
        releaseTCO = decayTCO;
    }
}

void Envelope::onAttackTimeChange( Signal v )
{

    // --- samples for the exponential rate
    double samples = getSampleRate()*( ( attackTimeScalar*double( v ) ) / 1000.0 );

    // --- coeff and base for iterative exponential calculation
    attackCoeff = exp( -log( ( 1.0 + attackTCO ) / attackTCO ) / samples );
    attackOffset = ( 1.0 + attackTCO )*( 1.0 - attackCoeff );
}

void Envelope::onDecayTimeChange( Signal v )
{

    // --- samples for the exponential rate
    double samples = getSampleRate()*( ( decayTimeScalar*double( v ) ) / 1000.0 );

    // --- coeff and base for iterative exponential calculation
    decayCoeff = exp( -log( ( 1.0 + decayTCO ) / decayTCO ) / samples );
    decayOffset = ( getSustainLevel() - decayTCO )*( 1.0 - decayCoeff );
}

void Envelope::onReleaseTimeChange( Signal v )
{
    // --- samples for the exponential rate
    double samples = getSampleRate()*( ( decayTimeScalar*double( v ) ) / 1000.0 );

    // --- coeff and base for iterative exponential calculation
    releaseCoeff = exp( -log( ( 1.0 + releaseTCO ) / releaseTCO ) / samples );
    releaseOffset = -releaseTCO*( 1.0 - releaseCoeff );
}

Envelope::signal Envelope::renderInternal(Signal input)
{
    switch ( state )
    {
        case OFF:
            if ( resetToZero ) envelope = 0.0;
            break;
        case ATTACK:
            envelope = attackOffset + envelope*attackCoeff;
            if ( envelope >= 1.0 || attackTimeScalar*getAttackTime() <= 0.0 )
            {
                envelope = 1.0;
                state = DECAY;
            }
            break;
        case DECAY:
            envelope = decayOffset + envelope*decayCoeff;
            if ( envelope <= getSustainLevel() || decayTimeScalar*getDecayTime() <= 0.0 )
            {
                envelope = getSustainLevel();
                state = SUSTAIN;
            }
            break;
        case SUSTAIN:
            envelope = getSustainLevel();
            break;
        case RELEASE:
            envelope = releaseOffset + envelope*releaseCoeff;
            if ( envelope <= 0.0 || getReleaseTime() <= 0.0 )
            {
                envelope = 0.0;
                state = OFF;
            }
            break;
    }
    return Signal( envelope, envelope );
}

void Envelope::start()
{
    attackTimeScalar = 1.0;
    decayTimeScalar = 1.0;
    envelope = 0.0;
    state = ATTACK;
}

void Envelope::stop()
{
    if ( envelope > 0.0 )
        state = RELEASE;
    else
        state = OFF;
}

void Envelope::turnOff()
{
    envelope = 0.0;
    state = OFF;
}
