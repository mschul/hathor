#include "Delay.h"
using namespace hathor;

void Delay::init( int channels, int sampleRate, SynthComponent::Ptr delay, SynthComponent::Ptr feedback, SynthComponent::Ptr wet )
{
    SynthComponent::init( sampleRate );
    {
        param* p = new param( delay );
        delayIdx = params.size();
        params.push_back( std::unique_ptr<param>( p ) );
    }
    {
        param* p = new param( feedback );
        feedbackIdx = params.size();
        params.push_back( std::unique_ptr<param>( p ) );
    }
    {
        param* p = new param( wet );
        wetIdx = params.size();
        params.push_back( std::unique_ptr<param>( p ) );
    }
    delayBuffer.clear();
    delayBuffer.setSize( channels, this->sampleRate * 2 );
    pos = 0;
}

void Delay::render( AudioSampleBuffer& buffer, int numSamples )
{
    int rp = 0;
    int wp = 0;

    double delay = getDelay() * sampleRate * 0.001;

    if ( delay == 0.0 )
        return;

    double wet = getWet();
    double feedback = getFeedback();

    // Apply our delay effect to the new output..
    for ( int channel = 0; channel < delayBuffer.getNumChannels(); ++channel )
    {
        float* channelData = buffer.getWritePointer( channel );
        float* delayData = delayBuffer.getWritePointer( jmin( channel, delayBuffer.getNumChannels() - 1 ) );
        wp = pos;
        rp = pos - ( int )delay;
        if ( rp < 0 )
            rp += delayBuffer.getNumSamples();

        for ( int i = 0; i < numSamples; ++i )
        {
            const double xn = channelData[i];

            float yn0 = delayData[rp];
            int rp1 = rp - 1;
            if ( rp1 < 0 )
                rp1 = delayBuffer.getNumSamples() - 1;
            float yn1 = delayData[rp1];
            const double yn = lerp( delay - ( int )delay, yn0, yn1 );

            channelData[i] = float( wet*yn + ( 1.0 - wet )*xn );
            delayData[wp] = float( xn + feedback * yn );

            if ( ++rp >= delayBuffer.getNumSamples() )
                rp = 0;
            if ( ++wp >= delayBuffer.getNumSamples() )
                wp = 0;
        }
    }

    pos = wp;
}
