#ifndef INPUT_H_INCLUDED
#define INPUT_H_INCLUDED

namespace hathor
{
template<typename Component>
class WeightedInput
{
public:
    WeightedInput(typename Component::Ptr source, double weight)
        : source(source), weight(weight)
    {}

    typename Component::Ptr getSource() const
    {
        return source;
    }

    void setSource(typename Component::Ptr source)
    {
        this.source = source;
    }

    double getWeight() const
    {
        return weight;
    }

    void setWeight(double weight)
    {
        this.weight = weight;
    }

private:
    typename Component::Ptr source;
    double weight;
};
}

#endif
