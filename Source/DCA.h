#include "../JuceLibraryCode/JuceHeader.h"

#include "SynthComponent.h"

#ifndef DCA_H_INCLUDED
#define DCA_H_INCLUDED

namespace hathor
{
    class DCA : public SynthComponent
    {
    public:
        typedef std::shared_ptr<DCA> Ptr;

        DCA() {};

        void init( int sampleRate, SynthComponent::Ptr pan, SynthComponent::Ptr amplitude );

        double getPan() const { return params[panIdx]->getSignal().left; }
        double getAmplitude() const { return params[amplitudeIdx]->getSignal().left; }

    protected:
        virtual signal renderInternal(Signal input) override;
        int panIdx;
        int amplitudeIdx;
    };
}

#endif
