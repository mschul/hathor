#include "../JuceLibraryCode/JuceHeader.h"

#include "Filter.h"

#ifndef VA_ONE_POLE_FILTER_H_INCLUDED
#define VA_ONE_POLE_FILTER_H_INCLUDED

namespace hathor
{
    class VAOnePoleFilter final : public Filter
    {
    public:
        typedef std::shared_ptr<VAOnePoleFilter> Ptr;

        VAOnePoleFilter() {};

        void init( int sampleRate, SynthComponent::Ptr cutoff, SynthComponent::Ptr filterType );

        void onCutoffChange( Signal v );

    protected:
        virtual signal renderInternal(Signal input) override;

        double alpha = 0.0;			// Feed Forward coeff
        double z1 = 0.0;					// z1 Storage
    };
}

#endif
