#include "Filter.h"
using namespace hathor;

void Filter::init( int sampleRate, SynthComponent::Ptr cutoff, SynthComponent::Ptr filterType )
{
    SynthComponent::init( sampleRate );
    {
        param* p = new param( cutoff );
        cutoffIdx = params.size();
        params.push_back( std::unique_ptr<param>( p ) );
    }
    {
        param* p = new param( filterType );
        filterTypeIdx = params.size();
        params.push_back( std::unique_ptr<param>( p ) );
    }
}
