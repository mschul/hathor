#include "../JuceLibraryCode/JuceHeader.h"

#include "SynthComponent.h"

#ifndef DELAY_H_INCLUDED
#define DELAY_H_INCLUDED

namespace hathor
{
    class Delay : public SynthComponent
    {
    public:
        typedef std::shared_ptr<Delay> Ptr;

        Delay() { }

        void init( int channels, int sampleRate, SynthComponent::Ptr delay, SynthComponent::Ptr feedback, SynthComponent::Ptr wet );

        virtual void render( AudioSampleBuffer& buffer, int numSamples );

        double getDelay() const { return params[delayIdx]->getSignal().left; }
        double getWet() const { return params[wetIdx]->getSignal().left; }
        double getFeedback() const { return params[feedbackIdx]->getSignal().left; }

    private:
        virtual signal renderInternal(Signal input) override { return lastOutput; }

        AudioSampleBuffer delayBuffer;
        int pos = 0;

        int delayIdx;
        int feedbackIdx;
        int wetIdx;
    };
}

#endif
