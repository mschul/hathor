#include "../JuceLibraryCode/JuceHeader.h"

#include <memory>
#include "Signal.h"
#include "WeightedInput.h"
#include "Param.h"

#ifndef SYNTH_COMPONENT_H_INCLUDED
#define SYNTH_COMPONENT_H_INCLUDED

namespace hathor
{
    class SynthComponent
    {
    public:
        using signal = Signal;
        using input = WeightedInput<SynthComponent>;
        using param = Param<SynthComponent>;

        typedef std::shared_ptr<SynthComponent> Ptr;

        // helper functions and misc classes, collect here for later refactoring!
        static double clamp( double v, double l, double h )
        {
            return std::max( std::min( h, v ), l );
        }
        static double lerp( double v, double l, double h )
        {
            return v*l + ( 1.0 - v )*h;
        }

        SynthComponent() : lastToken( -1 ), lastOutput( 0 ), sampleRate( 0 )
        {}

        virtual void init( int sampleRate )
        {
            setSampleRate( sampleRate );
        }

        void updateParams( int token )
        {
            for ( int i = params.size(); --i >= 0; )
                params[i]->update( token );
        }

        void setParam( int index, double newValue )
        {
            params[index]->setSignal( signal( newValue ) );
        }

        virtual signal render( int token )
        {
            if( token != lastToken )
            {
                updateParams( token );

                signal input;
                for ( int i = inputs.size(); --i >= 0; )
                    input += inputs[i]->getSource()->render( token ) * inputs[i]->getWeight();

                lastOutput = renderInternal( input );
            }
            return lastOutput;
        }

        void setSampleRate( int sampleRate ) { this->sampleRate = sampleRate; }
        double getSampleRate() const { return sampleRate; }

        void addInput( SynthComponent::Ptr component, double weight = 1.0 )
        {
            input* in = new input( component, weight );
            inputs.push_back( std::unique_ptr<input>( in ) );
        }

    protected:
        virtual signal renderInternal ( signal input ) = 0;

        std::vector<std::unique_ptr<input>> inputs;
        std::vector<std::unique_ptr<param>> params;

        int lastToken;
        signal lastOutput;

        double sampleRate;
    };
}

#endif
