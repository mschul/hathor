#include "DCA.h"
using namespace hathor;

void DCA::init( int sampleRate, SynthComponent::Ptr pan, SynthComponent::Ptr amplitude )
{
    SynthComponent::init( sampleRate );
    {
        param* p = new param( pan );
        panIdx = params.size();
        params.push_back( std::unique_ptr<param>( p ) );
    }
    {
        param* p = new param( amplitude );
        amplitudeIdx = params.size();
        params.push_back( std::unique_ptr<param>( p ) );
    }
}

DCA::signal DCA::renderInternal( Signal input )
{
    double pan = getPan();
    double amplitude = getAmplitude();

    double leftPan = cos( ( double_Pi / 4.0 )*( pan + 1.0 ) );
    double rightPan = sin( ( double_Pi / 4.0 )*( pan + 1.0 ) );

    leftPan = clamp( leftPan, 0.0, 1.0 );
    rightPan = clamp( rightPan, 0.0, 1.0 );

    return input * signal( leftPan, rightPan ) * amplitude;
}
