#include "LFO.h"
using namespace hathor;

void LFO::init( int sampleRate, SynthComponent::Ptr waveform, SynthComponent::Ptr frequency,
                SynthComponent::Ptr pulseWidth, SynthComponent::Ptr frequencyMod,
                SynthComponent::Ptr mode, SynthComponent::Ptr amplitude )
{
    Oscillator::init( sampleRate, waveform, frequency, pulseWidth, frequencyMod );
    {
        param* p = new param( amplitude );
        amplitudeIdx = params.size();
        params.push_back( std::unique_ptr<param>( p ) );
    }
    {
        param* p = new param( mode );
        modeIdx = params.size();
        params.push_back( std::unique_ptr<param>( p ) );
    }
}

void LFO::preRender( double x, bool wrapped, double& xQuad )
{
    xQuad = x + 0.25;
    if ( xQuad >= 1.0 )
        xQuad -= 1.0;

    if ( getMode() == SHOT && wrapped )
    {
        noteOn = false;
    }
}

Signal LFO::renderSine( double x, double /*dx*/, bool wrapped )
{
    double xQuad;
    preRender( x, wrapped, xQuad );
    return Signal(
               sin( x * 2 * double_Pi ),
               sin( xQuad * 2 * double_Pi ) ) * getAmplitude();
}
Signal LFO::renderSaw( double x, double /*dx*/, bool wrapped )
{
    double xQuad;
    preRender( x, wrapped, xQuad );
    if ( getMode() != SHOT )
    {
        return Signal( 2 * x - 1, 2 * xQuad - 2 ) * getAmplitude();
    }
    else
    {
        return Signal( x - 1, xQuad - 2 ) * getAmplitude();
    }
}
Signal LFO::renderSquare( double x, double /*dx*/, bool wrapped )
{
    double xQuad;
    preRender( x, wrapped, xQuad );

    double width = this->getPulseWidth();
    return Signal( x > width ? -1.0 : +1.0,
                   xQuad > width ? -1.0 : +1.0 ) * getAmplitude();
}
Signal LFO::renderTriangle( double x, double /*dx*/, bool wrapped )
{
    double out, outQuad, xQuad;
    preRender( x, wrapped, xQuad );

    out = ( x < 0.5 ) ? 2.0*x : 1.0 - 2.0*( x - 0.5 );
    outQuad = ( xQuad < 0.5 ) ? 2.0*xQuad : 1.0 - 2.0*( xQuad - 0.5 );
    if ( getMode() != SHOT )
    {
        out = 2 * out - 1;
        outQuad = 2 * outQuad - 1;
    }
    return Signal( out, outQuad ) * getAmplitude();
}
