#include "../JuceLibraryCode/JuceHeader.h"

#include "Oscillator.h"

#ifndef BLOSCILLATOR_H_INCLUDED
#define BLOSCILLATOR_H_INCLUDED

namespace hathor
{
    class BLOscillator final : public Oscillator
    {
    public:
        typedef std::shared_ptr<BLOscillator> Ptr;

        BLOscillator() {}

        void init( int sampleRate, SynthComponent::Ptr waveform, SynthComponent::Ptr frequency,
                   SynthComponent::Ptr pulseWidth, SynthComponent::Ptr frequencyMod )
        {
            Oscillator::init( sampleRate, waveform, frequency, pulseWidth, frequencyMod );
        }

        virtual void start() override;

    private:
        virtual Signal renderSine( double x, double dx, bool /*wrapped*/ );
        virtual Signal renderSaw( double x, double dx, bool /*wrapped*/ );
        virtual Signal renderTriangle( double x, double dx, bool /*wrapped*/ );
        virtual Signal renderSquare( double x, double dx, bool /*wrapped*/ );

        double squaredSawZ1 = 0.0;
    };
}

#endif
