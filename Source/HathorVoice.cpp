#include "HathorVoice.h"
#include "BLOscillator.h"
#include "VAOnePoleFilter.h"
using namespace hathor;

HathorVoice::HathorVoice()
{
    token = 0;

    params.resize( TOTAL_NUM_PARAMETERS );
    params[CONSTANT_ZERO].reset( new hathor::Value( 0.0 ) );
    params[FREQUENCY].reset( new hathor::Value( 0.0 ) );

    params[LFO1_WAVEFORM].reset( new hathor::Value( Oscillator::SINE ) );
    params[LFO1_FREQUENCY].reset( new hathor::Value( 5.0 ) );
    params[LFO1_PULSEWIDTH].reset( new hathor::Value( 0.5 ) );
    params[LFO1_MODE].reset( new hathor::Value( LFO::SYNC ) );
    params[LFO1_AMPLITUDE].reset( new hathor::Value( 1.0 ) );

    lfos.resize( 1 );
    lfos[0].reset( new LFO() );
    lfos[0]->init( int( getSampleRate() ), params[LFO1_WAVEFORM], params[LFO1_FREQUENCY],
                    params[LFO1_PULSEWIDTH], params[CONSTANT_ZERO], params[LFO1_MODE], params[LFO1_AMPLITUDE] );

    params[OSC1_WAVEFORM].reset( new hathor::Value( Oscillator::SAW ) );
    params[OSC1_PULSEWIDTH].reset( new hathor::Value( 0.5 ) );
    params[OSC2_WAVEFORM].reset( new hathor::Value( Oscillator::SAW ) );
    params[OSC2_PULSEWIDTH].reset( new hathor::Value( 0.5 ) );

    oscillators.resize( 2 );
    oscillators[0].reset( new BLOscillator() );
    oscillators[0]->init( int( getSampleRate() ), params[OSC1_WAVEFORM], params[FREQUENCY], params[OSC1_PULSEWIDTH], lfos[0] );
    oscillators[1].reset( new BLOscillator() );
    oscillators[1]->init( int( getSampleRate() ), params[OSC2_WAVEFORM], params[FREQUENCY], params[OSC2_PULSEWIDTH], params[CONSTANT_ZERO] );

    filters.resize( 1 );
    params[FILTER_TYPE].reset( new hathor::Value( Filter::LPF ) );
    params[FILTER_CUTOFF].reset( new hathor::Value( 800.0 ) );
    filters[0].reset( new VAOnePoleFilter() );
    filters[0]->init( int( getSampleRate() ), params[FILTER_CUTOFF], params[FILTER_TYPE] );

    envelopes.resize( 1 );
    params[ENVELOPE_MODE].reset( new hathor::Value( Envelope::ANALOG ) );
    params[ENVELOPE_ATTACK_TIME].reset( new hathor::Value( 1000.0 ) );
    params[ENVELOPE_DECAY_TIME].reset( new hathor::Value( 1000.0 ) );
    params[ENVELOPE_SUSTAIN_LEVEL].reset( new hathor::Value( 1.0 ) );
    params[ENVELOPE_RELEASE_TIME].reset( new hathor::Value( 1000.0 ) );
    envelopes[0].reset( new Envelope() );
    envelopes[0]->init( int( getSampleRate() ), params[ENVELOPE_MODE], params[ENVELOPE_ATTACK_TIME], params[ENVELOPE_DECAY_TIME],
                         params[ENVELOPE_SUSTAIN_LEVEL], params[ENVELOPE_RELEASE_TIME] );

    params[DCA_PAN].reset( new hathor::Value( 0.5 ) );
    dca.reset( new DCA() );
    dca->init( int( getSampleRate() ), params[DCA_PAN], envelopes[0] );
}
HathorVoice::~HathorVoice()
{
}

bool HathorVoice::canPlaySound( SynthesiserSound* sound )
{
    return dynamic_cast<HathorSound*> ( sound ) != nullptr;
}

void HathorVoice::startNote( int midiNoteNumber, float /*velocity*/,
                             SynthesiserSound* /*sound*/,
                             int /*currentPitchWheelPosition*/ )
{
    params[FREQUENCY]->setValue( MidiMessage::getMidiNoteInHertz( midiNoteNumber ) );
    envelopes.front()->start();
    oscillators.front()->start();
    lfos.front()->start();

    // registers for triangles
}

void HathorVoice::stopNote( float /*velocity*/, bool allowTailOff )
{
    if ( allowTailOff )
    {
        envelopes.front()->stop();
    }
    else
    {
        clearCurrentNote();

        envelopes.front()->turnOff();
        oscillators.front()->stop();
        lfos.front()->stop();
    }
}

void HathorVoice::pitchWheelMoved( int /*newValue*/ )
{
}

void HathorVoice::controllerMoved( int /*controllerNumber*/, int /*newValue*/ )
{
}

void HathorVoice::renderNextBlock( AudioSampleBuffer& outputBuffer, int startSample, int numSamples )
{
    if ( envelopes.front()->getState() != Envelope::OFF )
    {
        while ( --numSamples >= 0 )
        {
            const SynthComponent::signal currentSample = dca->render(token);
            token = (token+1)%2;

            if ( outputBuffer.getNumChannels() == 2 )
            {
                outputBuffer.addSample( 0, startSample, float( currentSample.left ) );
                outputBuffer.addSample( 1, startSample, float( currentSample.right ) );
            }
            else if ( outputBuffer.getNumChannels() == 1 )
            {
                outputBuffer.addSample( 0, startSample, float( currentSample.toMono() ) );
            }
            else
            {
                float value = float( currentSample );
                for ( int i = outputBuffer.getNumChannels(); --i >= 0; )
                    outputBuffer.addSample( i, startSample, value );
            }

            ++startSample;
        }
    }
    else
        clearCurrentNote();
}

