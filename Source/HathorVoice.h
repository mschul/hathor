#include "../JuceLibraryCode/JuceHeader.h"

#include "Oscillator.h"
#include "Envelope.h"
#include "Filter.h"
#include "Value.h"
#include "LFO.h"
#include "DCA.h"

#ifndef HATHORVOICE_H_INCLUDED
#define HATHORVOICE_H_INCLUDED

namespace hathor
{
    class HathorSound : public SynthesiserSound
    {
    public:
        HathorSound() {}

        bool appliesToNote( int /*midiNoteNumber*/ ) override { return true; }
        bool appliesToChannel( int /*midiChannel*/ ) override { return true; }
    };

    class HathorVoice : public SynthesiserVoice
    {
    public:
        typedef std::shared_ptr<HathorVoice> Ptr;
        enum Parameters
        {
            CONSTANT_ZERO = 0,
            FREQUENCY,
            LFO1_FREQUENCY, LFO1_WAVEFORM, LFO1_PULSEWIDTH, LFO1_AMPLITUDE, LFO1_MODE,
            OSC1_WAVEFORM, OSC1_PULSEWIDTH,
            OSC2_WAVEFORM, OSC2_PULSEWIDTH,
            FILTER_TYPE, FILTER_CUTOFF,
            ENVELOPE_MODE, ENVELOPE_ATTACK_TIME, ENVELOPE_DECAY_TIME, ENVELOPE_SUSTAIN_LEVEL, ENVELOPE_RELEASE_TIME,
            DCA_PAN,
            TOTAL_NUM_PARAMETERS,
        };

        HathorVoice();
        virtual ~HathorVoice();

        bool canPlaySound( SynthesiserSound* sound ) override;
        void startNote( int midiNoteNumber, float velocity,
                        SynthesiserSound* sound,
                        int currentPitchWheelPosition ) override;
        void stopNote( float velocity, bool allowTailOff ) override;
        void pitchWheelMoved( int newValue ) override;
        void controllerMoved( int controllerNumber, int newValue ) override;
        void renderNextBlock( AudioSampleBuffer& outputBuffer, int startSample, int numSamples ) override;

        void setParameter( Parameters param, double newValue ) { params[param]->setValue( newValue ); }

    private:
        std::vector<hathor::Value::Ptr> params;

        int token;
        std::vector<Oscillator::Ptr> oscillators;
        std::vector<LFO::Ptr> lfos;
        std::vector<Envelope::Ptr> envelopes;
        std::vector<Filter::Ptr> filters;
        DCA::Ptr dca;
    };
}

#endif
