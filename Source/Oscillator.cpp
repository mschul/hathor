#include "Oscillator.h"
using namespace hathor;

void Oscillator::init( int sampleRate, SynthComponent::Ptr waveform, SynthComponent::Ptr frequency,
                       SynthComponent::Ptr pulseWidth, SynthComponent::Ptr frequencyMod )

{
    SynthComponent::init( sampleRate );
    {
        param* p = new param( waveform );
        waveformIdx = params.size();
        params.push_back( std::unique_ptr<param>( p ) );
    }
    {
        param* p = new param( frequency );
        frequencyIdx = params.size();
        params.push_back( std::unique_ptr<param>( p ) );
    }
    {
        param* p = new param( pulseWidth );
        pulseWidthIdx = params.size();
        params.push_back( std::unique_ptr<param>( p ) );
    }
    {
        param* p = new param( frequencyMod );
        frequencyModIdx = params.size();
        params.push_back( std::unique_ptr<param>( p ) );
    }
}

Oscillator::signal Oscillator::renderInternal(Signal input)
{
    if ( !noteOn )
        return Signal( 0 );

    double pitchShift = getFrequencyMod() + pitchBend + octave*12.0 + semitones + cents / 100.0;
    double frequency = getFrequency() * frequencyRatio * ( pitchShift == 0 ? 1.0 : pow( 2.0, pitchShift / 12.0 ) );

    double inc = frequency / getSampleRate();

    modulo += inc;
    bool wrapped = modulo > 1.0;
    if ( wrapped )
    {
        modulo = modulo - 1.0;
        modPhase *= -1;
    }

    Signal result;
    Waveform form = getWaveform();
    switch ( form )
    {
        case SINE:
            result = renderSine( modulo, inc, wrapped );
            break;
        case TRIANGLE:
            result = renderTriangle( modulo, inc, wrapped );
            break;
        case SAW:
            result = renderSaw( modulo, inc, wrapped );
            break;
        case SQUARE:
            result = renderSquare( modulo, inc, wrapped );
            break;
        default:
            result = renderSaw( modulo, inc, wrapped );
            break;
    }

    return result;
}

void Oscillator::start()
{
    // SAW and TRIANGLE are zero at mModulo = 0.5
    if ( getWaveform() == SAW || getWaveform() == TRIANGLE ) modulo = 0.5;
    // SIN and SQUARE are zero at mModulo = 0.0
    else modulo = 0.0;

    modPhase = -1;

    noteOn = true;
}

void Oscillator::stop()
{
    noteOn = false;
}

// see http://www.kvraudio.com/forum/viewtopic.php?t=375517
double Oscillator::polyBLEP( double x, double dx )
{
    double result = 0;
    if ( x > 1.0 - dx )
    {
        double t = ( x - 1.0 ) / dx;
        result = t*t + 2.0*t + 1.0;
    }
    else if ( x < dx )
    {
        double t = x / dx;
        result = -t*t + 2.0*t - 1.0;
    }
    return result;
}
