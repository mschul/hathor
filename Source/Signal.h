#include "../JuceLibraryCode/JuceHeader.h"

#ifndef SIGNAL_H_INCLUDED
#define SIGNAL_H_INCLUDED

namespace hathor
{
	class Signal
	{
	public:
		Signal() : left(0.0), right(0.0) {}
		explicit Signal(double left) : left(left), right(left) {}
		explicit Signal(double left, double right) : left(left), right(right) {}

		inline explicit operator double() const { return 0.5 * (left + right); }
		inline explicit operator float() const { return 0.5f * float(left + right); }
		inline double toMono(double mix = 0.5) const { return mix * left + 1 - mix * right; }

		inline void operator += (const Signal& other) { left += other.left; right += other.right; }
		inline void operator -= (const Signal& other) { left -= other.left; right -= other.right; }
		inline void operator *= (const Signal& other) { left *= other.left; right *= other.right; }
		inline void operator /= (const Signal& other) { left /= other.left; right /= other.right; }
		inline void operator += (double other) { left += other; right += other; }
		inline void operator -= (double other) { left -= other; right -= other; }
		inline void operator *= (double other) { left *= other; right *= other; }
		inline void operator /= (double other) { left /= other; right /= other; }

		inline bool operator != (const Signal& other) const { return (left != other.left || right != other.right); }
		inline bool operator == (const Signal& other) const { return (left == other.left && right == other.right); }

		inline Signal operator - () const { return Signal(-left, -right); }

		inline Signal operator + (const Signal& other) const { return Signal(left + other.left, right + other.right); }
		inline Signal operator - (const Signal& other) const { return Signal(left - other.left, right - other.right); }
		inline Signal operator * (const Signal& other) const { return Signal(left * other.left, right * other.right); }
		inline Signal operator / (const Signal& other) const { return Signal(left / other.left, right / other.right); }
		inline Signal operator + (double other) const { return Signal(left + other, right + other); }
		inline Signal operator - (double other) const { return Signal(left - other, right - other); }
		inline Signal operator * (double other) const { return Signal(left * other, right * other); }
		inline Signal operator / (double other) const { return Signal(left / other, right / other); }

		double left, right;
	};

	inline Signal operator + (double l, Signal& r) { return r + l; }
	inline Signal operator - (double l, Signal& r) { return -r + l; }
	inline Signal operator * (double l, Signal& r) { return r * l; }
	inline Signal operator / (double l, Signal& r) { return Signal(l / r.left, l / r.right); }
}

#endif