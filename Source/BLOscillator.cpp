#include "BLOscillator.h"
using namespace hathor;

// MAKE THIS http://www.virus.info/forum/index.php/Thread/2934-Vintage-Oscillators/

void BLOscillator::start()
{
    Oscillator::start();

    squaredSawZ1 = 0.0;
}

Signal BLOscillator::renderSine( double x, double /*dx*/, bool /*wrapped*/ )
{
    return Signal( sin( x * 2 * double_Pi ) );
}
Signal BLOscillator::renderSaw( double x, double dx, bool /*wrapped*/ )
{
    double saw = 2 * x - 1;
    if ( saw > 0.0 ) saw = tanh( 3 * saw ) / tanh( 3 );

    return Signal( saw ) - polyBLEP( x, dx );
}
Signal BLOscillator::renderSquare( double x, double dx, bool /*wrapped*/ )
{
    double out = double( renderSaw( x, dx, false ) );
    double width = getPulseWidth();

    double mod2 = x + width;
    mod2 = mod2 > 1.0 ? mod2 - 1.0 : mod2;

    out -= double( renderSaw( mod2, dx, false ) );
    out *= 0.5;

    double correction = ( width < 0.5 ) ? 1 - width : width;
    out /= correction;

    return Signal( out );
}
Signal BLOscillator::renderTriangle( double x, double dx, bool /*wrapped*/ )
{
    double saw = double( renderSaw( x, dx, false ) );

    double squaredSaw = saw * saw * modPhase;
    double c = sampleRate / ( 4.0*getFrequency()*( 1 - dx ) );
    double out = ( squaredSaw - squaredSawZ1 ) * c;
    squaredSawZ1 = squaredSaw;

    return Signal( out );
}
