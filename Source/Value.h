#include "../JuceLibraryCode/JuceHeader.h"

#include "SynthComponent.h"

#ifndef VALUE_H_INCLUDED
#define VALUE_H_INCLUDED

namespace hathor
{
    class Value : public SynthComponent
    {
    public:
        typedef std::shared_ptr<Value> Ptr;

        Value( double value )
            : value( value )
        {
        }

        void setValue( double value ) { this->value = value; }

    protected:
        virtual signal renderInternal(Signal input) override
        {
            return Signal( value );
        }

    private:
        double value;
    };
}

#endif
