#include "../JuceLibraryCode/JuceHeader.h"

#include "Oscillator.h"

#ifndef LFO_H_INCLUDED
#define LFO_H_INCLUDED

namespace hathor
{
    class LFO final : public Oscillator
    {
    public:
        typedef std::shared_ptr<LFO> Ptr;
        enum Mode { SYNC, SHOT, FREE };

        LFO() {}

        void init( int sampleRate, SynthComponent::Ptr waveform, SynthComponent::Ptr frequency,
                   SynthComponent::Ptr pulseWidth, SynthComponent::Ptr frequencyMod,
                   SynthComponent::Ptr mode, SynthComponent::Ptr amplitude );

        Mode getMode() const { return ( Mode )int( params[modeIdx]->getSignal().left ); }
        double getAmplitude() const { return params[amplitudeIdx]->getSignal().left; }

    private:
        int modeIdx;
        int amplitudeIdx;

        virtual Signal renderSine( double x, double dx, bool wrapped );
        virtual Signal renderSaw( double x, double dx, bool wrapped );
        virtual Signal renderTriangle( double x, double dx, bool wrapped );
        virtual Signal renderSquare( double x, double dx, bool wrapped );

        void preRender( double x, bool wrapped, double& xQuad );
    };
}

#endif
