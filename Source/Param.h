#include "../JuceLibraryCode/JuceHeader.h"

#include "Signal.h"

#ifndef PARAM_H_INCLUDED
#define PARAM_H_INCLUDED

namespace hathor
{
template<typename Component>
class Param
{
public:
    typedef void (Component::*OnChangeCallbackPtr)(Signal);

    Param(typename Component::Ptr source)
        : source(source), onChange(nullptr)
    {
    }

    virtual ~Param() {}

    void update(int token)
    {
        Signal v = source->render(token);
        if (v != signal) setSignal(v);
    }

    void setSource(typename Component::Ptr source)
    {
        this->source = source;
    }

    void setOnChangeCallback(OnChangeCallbackPtr callback) {
        onChange = callback;
    }

    typename Component::Ptr getSource() const
    {
        return signal;
    }

    virtual void setSignal(const Signal& signal)
    {
        this->signal = signal;
        if(onChange != nullptr)
            ((*source).*onChange)(signal);
    }

    virtual Signal getSignal() const
    {
        return signal;
    }

private:
    typename Component::Ptr source;
    OnChangeCallbackPtr onChange;
    Signal signal;
};
}
#endif
