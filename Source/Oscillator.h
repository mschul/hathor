#ifndef OSCILLATOR_H_INCLUDED
#define OSCILLATOR_H_INCLUDED

#include "SynthComponent.h"

namespace hathor
{
    class Oscillator : public SynthComponent
    {
    public:
        typedef std::shared_ptr<Oscillator> Ptr;
        enum Waveform { SINE, SAW, TRIANGLE, SQUARE };

        Oscillator() {};

        void init( int sampleRate, SynthComponent::Ptr waveform, SynthComponent::Ptr frequency,
                   SynthComponent::Ptr pulseWidth, SynthComponent::Ptr frequencyMod );

        virtual void start();
        virtual void stop();
        inline bool isPlaying() { return noteOn; }

        Waveform getWaveform() const { return ( Waveform )int( params[waveformIdx]->getSignal().left ); }
        double getPulseWidth() const { return params[pulseWidthIdx]->getSignal().left; }
        double getFrequencyMod() const { return params[frequencyModIdx]->getSignal().left; }
        double getFrequency() const { return params[frequencyIdx]->getSignal().left; }

    protected:
        virtual signal renderInternal(Signal input) override;

        int waveformIdx;
        int pulseWidthIdx;
        int frequencyModIdx;
        int frequencyIdx;

        virtual Signal renderSine( double x, double dx, bool wrapped ) = 0;
        virtual Signal renderSaw( double x, double dx, bool wrapped ) = 0;
        virtual Signal renderTriangle( double x, double dx, bool wrapped ) = 0;
        virtual Signal renderSquare( double x, double dx, bool wrapped ) = 0;

        double polyBLEP( double x, double dx );

        double frequencyRatio = 1.0;

        int octave = 0;
        int semitones = 0;
        int cents = 0;

        double pitchBend = 0.0;

        double modulo = 0.0;
        int modPhase = -1;
        bool noteOn = false;
    };
}

#endif
