#include "VAOnePoleFilter.h"
using namespace hathor;

void VAOnePoleFilter::init( int sampleRate, SynthComponent::Ptr cutoff, SynthComponent::Ptr filterType )

{
    Filter::init( sampleRate, cutoff, filterType );

    params[cutoffIdx]->setOnChangeCallback( static_cast<param::OnChangeCallbackPtr>( &VAOnePoleFilter::onCutoffChange ) );
}

void VAOnePoleFilter::onCutoffChange( Signal v )
{
    double sr = getSampleRate();

    double wd = 2.0 * double_Pi * double( v );
    double T = 1 / sr;
    double wa = ( 2 / T )*tan( wd*T / 2 );
    double g = wa*T / 2;

    alpha = g / ( 1.0 + g );
}

VAOnePoleFilter::signal VAOnePoleFilter::renderInternal(Signal input)
{
    double xn = double( input );

    // calculate v(n)
    double vn = ( xn - z1 ) * alpha;

    // form LP output
    double lpf = vn + z1;

    // update memory
    z1 = vn + lpf;

    // do the HPF
    double hpf = xn - lpf;

    FilterType type = getFilterType();
    if ( type == LPF )
        return signal( lpf );
    else if ( type == HPF )
        return signal( hpf );

    return signal( xn ); // should never get here
}
