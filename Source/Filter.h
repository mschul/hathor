#include "../JuceLibraryCode/JuceHeader.h"

#include "SynthComponent.h"

#ifndef FILTER_H_INCLUDED
#define FILTER_H_INCLUDED

namespace hathor
{
    class Filter : public SynthComponent
    {
    public:
        typedef std::shared_ptr<Filter> Ptr;
        enum FilterType { LPF, HPF, BPF };

        Filter() {}

        void init( int sampleRate, SynthComponent::Ptr cutoff, SynthComponent::Ptr filterType );

        FilterType getFilterType() const { return ( FilterType )int( params[filterTypeIdx]->getSignal().left ); }
        void setFilterType( FilterType type ) { params[filterTypeIdx]->setSignal( Signal( type ) ); }

        double getCutoff() const { return params[cutoffIdx]->getSignal().left; }
        void setCutoff( double value ) { params[cutoffIdx]->setSignal( Signal( value ) ); }

    protected:
        int filterTypeIdx;
        int cutoffIdx;
    };
}

#endif
